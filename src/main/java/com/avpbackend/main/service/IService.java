package com.avpbackend.main.service;

import com.avpbackend.main.dto.Employee;

public interface IService {

	public Employee saveEmployeeData(Employee employee);
}
