package com.avpbackend.main.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.support.IsNewStrategy;
import org.springframework.stereotype.Service;

import com.avpbackend.main.dto.Employee;
import com.avpbackend.main.repo.EmployeeRepo;
import com.avpbackend.main.service.IService;

@Service
public class Iserviceimpl implements IService {

	@Autowired
	private EmployeeRepo repo;

	@Override
	public Employee saveEmployeeData(Employee employee) {
		// TODO Auto-generated method stub

		return repo.save(employee);
	}

}
