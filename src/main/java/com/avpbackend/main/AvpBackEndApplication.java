package com.avpbackend.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvpBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvpBackEndApplication.class, args);
	}

}
