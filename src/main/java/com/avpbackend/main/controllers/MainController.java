package com.avpbackend.main.controllers;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.avpbackend.main.dto.Employee;
import com.avpbackend.main.service.IService;

@Controller
public class MainController {

	@Autowired
	private IService service;

	@RequestMapping(value = "/index")
	public ResponseEntity<Employee> index() {
		return new ResponseEntity<Employee>(new Employee(1, "onefname", "twolname"), HttpStatus.ACCEPTED);
	}

	@PostMapping("/save")
	public ResponseEntity<Employee> saveEmployeeData(@RequestBody Employee employee) {
		return new ResponseEntity<Employee>(service.saveEmployeeData(employee), HttpStatus.OK);
	}

}
