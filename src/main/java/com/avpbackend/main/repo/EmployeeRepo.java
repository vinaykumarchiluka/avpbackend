package com.avpbackend.main.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.avpbackend.main.dto.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer>{

}
